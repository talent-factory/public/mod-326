package patterns.proxy;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

@SuppressWarnings("all")
public class Proxy implements Subject {

    private final String host = "127.0.0.1";
    private final int port = 8765;

    private BufferedReader in;
    private PrintWriter out;

    public Proxy() {
        try {
            Socket socket = new Socket(host, port);
            in = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(
                    socket.getOutputStream(), true);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    @Override
    public boolean signon(String user) {
        boolean result = false;
        try {
            out.println("signon");
            result = "ok".equals(in.readLine());
        }
        catch (Exception ignored) {}
        finally {
            return result;
        }
    }
}
