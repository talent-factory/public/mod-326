package patterns.proxy;

public interface Subject {
    boolean signon(String user);
}
