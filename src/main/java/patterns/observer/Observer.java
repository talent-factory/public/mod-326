package patterns.observer;

import java.awt.*;
import java.util.EventObject;

public interface Observer {
    void update();
}
