package patterns.observer;

import java.util.HashSet;
import java.util.Set;

public class Notifier implements Observer {

    private Set<Observer> observers = new HashSet<>();

    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public void removeObserevr(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void update() {

    }
}
