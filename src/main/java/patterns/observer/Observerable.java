package patterns.observer;

public class Observerable {

    private final String name;

    public Observerable(String name) {
        this.name = name;
    }
}
