package patterns.factory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

public class EncryptedSocket extends Socket {

    private static Encryption crypt;
    private Key key;

    public EncryptedSocket(Key key, EncryptionFactory factory)
        throws NoSuchAlgorithmException {

        this.key = key;
        crypt = factory.createEncryption(key);
    }

    /**
     * Return an intput stream that decrypts the inbound stream
     * of bytes.
     */
    public InputStream getInputStream() throws IOException {
        return crypt.decryptedInputStream(super.getInputStream());
    }

    /**
     * Return an output stream that entcrypts the outbound stream
     * of bytes.
     */
    public OutputStream getOutputStream() throws IOException {
        return crypt.encryptedOutputStream(super.getOutputStream());
    }
}
