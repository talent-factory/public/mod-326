package patterns.factory;

import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;

public class RSAEncryption extends Encryption {

    public RSAEncryption(Key key) {
        super(key);
    }

    @Override
    OutputStream encryptedOutputStream(OutputStream out) {
        return null;
    }

    @Override
    InputStream decryptedInputStream(InputStream in) {
        return null;
    }
}
