package patterns.factory;

import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;

public class DESEncryption extends Encryption {

    public DESEncryption(Key key) {
        super(key);
    }

    @Override
    OutputStream encryptedOutputStream(OutputStream out) {
        return null;
    }

    @Override
    InputStream decryptedInputStream(InputStream in) {
        return null;
    }
}
