package patterns.factory;

import java.security.Key;
import java.security.NoSuchAlgorithmException;

public interface EncryptionFactory {

    Encryption createEncryption(Key key)
            throws NoSuchAlgorithmException;
}
