package patterns.factory;

import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;

@SuppressWarnings("FieldCanBeLocal")
abstract public class Encryption {

    private Key key;

    public Encryption(Key key) {
        this.key = key;
    }

    abstract InputStream decryptedInputStream(InputStream in);
    abstract OutputStream encryptedOutputStream(OutputStream out);
}
