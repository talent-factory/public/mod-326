package patterns.factory;

import java.security.Key;
import java.security.NoSuchAlgorithmException;

public class EncryptionFactoryImpl implements EncryptionFactory {

    @Override
    public Encryption createEncryption(Key key)
            throws NoSuchAlgorithmException {

        String algorithm = key.getAlgorithm();

        if ("DES".equals(algorithm))
            return new DESEncryption(key);

        if ("RSA".equals(algorithm))
            return new DESEncryption(key);

        throw new NoSuchAlgorithmException();
    }
}
