package book.ch02;

public class ArithmOp {
    public static void main(String[] args) {
        int a = 5;

        System.out.println(13 / 5);
        System.out.println(13 % 5);

        System.out.println(12. / 2.5);
        System.out.println(12. % 2.5);

        System.out.println(++a + " " + a);
        System.out.println(a++ + " " + a);

        System.out.println(--a + " " + a);
        System.out.println(a-- + " " + a);

        double x = 0.7 + 0.1;
        double y = 0.9 - 0.1;
        System.out.println(y - x);
    }
}
