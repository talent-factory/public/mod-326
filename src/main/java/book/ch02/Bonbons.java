package book.ch02;

public class Bonbons {
    public static void main(String[] args) {

        System.out.println("Fliesskommazahlen: ");
        {
            double budget = 1.;
            int anzahl = 0;

            for (double preis = 0.1; budget >= preis; preis += 0.1) {
                budget -= preis;
                anzahl++;
            }

            System.out.println(anzahl + " Bonbons gekauft.");
            System.out.println("Restgeld: " + budget);
        }

        System.out.println("----\nGanze Zahlen");
        {
            int budget = 100;
            int anzahl = 0;

            for (int preis = 10; budget >= preis; preis += 10) {
                budget -= preis;
                anzahl++;
            }

            System.out.println(anzahl + " Bonbons gekauft.");
            System.out.println("Restgeld: " + budget);
        }
    }
}
