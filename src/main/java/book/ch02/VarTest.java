package book.ch02;

public class VarTest {
    public static void main(String[] args) {
        // Variablendefinition mit Initialisierung
        boolean b = true;
        char c = 'x';
        int i = 4711;
        double d = 0.12345e1;
        int x = 0b01011111_10101010_00001111_11110000;
        double y = 3.141_592_653;

        // Ausgabe der Variablenwerte
        System.out.println("b: " + b);
        System.out.println("c: " + c);
        System.out.println("i: " + i);
        System.out.println("d: " + d);
        System.out.println("x: " + x);
        System.out.println("y: " + y);
    }
}
