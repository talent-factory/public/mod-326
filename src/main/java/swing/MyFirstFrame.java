package swing;

import javax.swing.*;
import java.awt.*;

public class MyFirstFrame extends JFrame {
    public MyFirstFrame() {
        super("Ein erstes Beispiel");

        Icon icon = new ImageIcon("src/main/resources/duke.gif");
        JLabel label = new JLabel("Viel Erfolg mit dem Grundkurs Java", icon,  JLabel.CENTER);
        add(label);

        Font schrift = new Font("SansSerif", Font.BOLD, 20);
        label.setFont(schrift);
        label.setForeground(Color.red);
        label.setBackground(Color.white);
        label.setOpaque(true);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(500, 200);
        setVisible(true);
    }

    public static void main(String[] args) {
        new MyFirstFrame();
    }
}
