package swing;

import javax.swing.*;
import java.awt.*;

public class GridBagExample extends JFrame {

    final static int GRID_SIZE = 15; // Must be odd
    final static boolean shouldWeightX = true;

    public static void addComponentsToPane(Container pane) {

        pane.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;

        Icon wall = new ImageIcon("src/main/resources/wall.png");
        Icon green = new ImageIcon("src/main/resources/green.png");

        JLabel button;
        for (int row = 0; row < GRID_SIZE; row++) {
            for (int col = 0; col < GRID_SIZE; col++) {

                button = new JLabel("", wall, JLabel.CENTER);
                if (row % 2 == 0 || col % 2 == 0 )
                    button.setIcon(green);

                button.setBackground(Color.GREEN);
                constraints.fill = GridBagConstraints.HORIZONTAL;
                constraints.gridx = col;
                constraints.gridy = row;
                pane.add(button, constraints);
            }
        }

    }

    /**
     * Create the GUI and show it.  For thread safety, this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {

        JFrame frame = new JFrame("GridBagLayoutDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        addComponentsToPane(frame.getContentPane());  // Set up the content pane.

        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
    }

    /**
     * Schedule a job for the event-dispatching thread: creating and showing this application's GUI.
     *
     * @param args There are no arguments to be processed.
     */
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(GridBagExample::createAndShowGUI);
    }
}
