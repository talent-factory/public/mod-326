package thread;

import java.util.Random;

public class MyThread extends Thread {

    private int threadCounter;
    private Random random = new Random();

    public void run() {
        String name = getName();
        for (int i = 0; i < 10; i++) {
            try {
                /*
                 * Jeder Thread schläft für eine zufällige Anzahl
                 * Sekunden und erledigt anschliessen seine
                 * Arbeit.
                 */
                Thread.sleep(random.nextInt(1_000));
                System.out.println(name + ": " + ++threadCounter);
            } catch (InterruptedException ignored) {
            }
        }
        System.out.println(name + ": Done.");
    }

    public static void main(String[] args) {

        /*
         * Wir erstellen 10 parallele Threads an dieser
         * Stelle, welche eigenständig ihre Arbeit ausführen.
         */ 
        for (int i = 0; i < 10; i++)
            new MyThread().start();

        System.out.println("All threads started...");
    }
}
